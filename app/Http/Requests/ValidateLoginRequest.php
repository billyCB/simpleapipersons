<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Http\JsonResponse;

class ValidateLoginRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [    
            'email' => 'required|string',
            'password' => 'required|string'
        ];
    }

    /**
    * Response for fail validation
    *
    * @return json
    */
    public function response(array $errors)
    {
        $response = ['state' => 'fail', 'messages' => $errors];
        return new JsonResponse($response, 422);
    }
}
