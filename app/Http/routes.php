<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'api'], function () {
    Route::post('/user/create', 'Api\UserController@createUser');
    Route::post('/person/create', 'Api\PersonController@createPerson');
    Route::post('/login', 'Api\AuthController@authUser');
	Route::get('/person/all', 'Api\PersonController@getAllPerson');
	Route::post('/person/sync', 'Api\PersonController@registerPersons');

});
