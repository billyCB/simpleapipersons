<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

class ApiBaseController extends Controller
{

    public function __construct(){
        //
    }

    /**
    * Response Methods
    */

    public function reponseSuccess($data = null){
        $response = [ "message" => "success", "data" => $data];
        return self::parseToJson($response);
    }

    public function reponseFail($data = null){
        $response = [ "message" => "fail", "data" => $data];
        return self::parseToJson($response);
    }

    public function parseToJson($response){
        return response()->json($response);
    }


}
