<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\NewUserRequest;
use App\Models\Person;
use App\User;
use Hash;
use Request;

class UserController extends ApiBaseController
{

    public function __construct(){
    }

    public function createUser(NewUserRequest $request)
    {
        $user = User::create($request->all());
        $user->password = md5($request["password"]);
        $user->save();
        return self::reponseSuccess($user);
    }
}
