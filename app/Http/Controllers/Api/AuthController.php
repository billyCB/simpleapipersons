<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ValidateLoginRequest;
use App\User;
use Request;
use Hash;

class AuthController extends ApiBaseController
{

    public function __construct(){
    }

    public function authUser(ValidateLoginRequest $request)
    {
        $user = User::whereEmail($request["email"])
                ->wherePassword(md5($request['password']))
                ->first();
    	if (is_null($user)) {
    		return self::reponseFail();
    	}else{
    		return self::reponseSuccess();
    	}        
    }
}
