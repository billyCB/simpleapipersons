<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\NewPersonRequest;
use Illuminate\Http\Request;
use App\Models\Person;
use App\User;

class PersonController extends ApiBaseController
{

    public function __construct(){
    }

    public function createPerson(NewPersonRequest $request)
    {
        $person = Person::create($request->all());
        $person->save();
        return self::reponseSuccess($person);
    }

    public function registerPersons(Request $request)
    {
        $persons = Person::insert($request->all());
        return self::reponseSuccess($persons);
    }

    public function getAllPerson()
    {
    	$persons = Person::all();
    	return self::reponseSuccess($persons);
    }
}
